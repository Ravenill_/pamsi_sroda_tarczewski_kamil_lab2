#include <iostream>

template<typename Type>
Queue<Type>::Queue()
{
	first = NULL;
	last = NULL;
}

template<typename Type>
Queue<Type>::~Queue()
{
	while (first)
		dequeue();
}

template<typename Type>
void Queue<Type>::enqueue(Type a)
{
	pushBack(a);
	first = List<Type>::getFirst();
	last = List<Type>::getLast();
}

template<typename Type>
Type Queue<Type>::dequeue()
{
	try
	{
		Node<Type> *temp = first;
		Type result;

		if (temp != NULL)
		{
			result = (*temp).getData();
			delFront();
			first = List<Type>::getFirst();
			last = List<Type>::getLast();
		}
		else
		{
			std::string w = "EmptyQueueException";
			throw w;
		}
		return result;
	}
	catch(std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template<typename Type>
Type Queue<Type>::front()
{
	try
	{
		Node<Type> *temp = first;
		Type result;

		if (temp != NULL)
			result = (*temp).getData();
		else
		{
			std::string w = "EmptyQueueException";
			throw w;
		}
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template<typename Type>
void Queue<Type>::remove()
{
	List<Type>::remove();

	first = List<Type>::getFirst();
	last = List<Type>::getLast();
}
