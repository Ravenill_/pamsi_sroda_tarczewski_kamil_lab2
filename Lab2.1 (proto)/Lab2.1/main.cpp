#include <iostream>
#include <cstdlib>
#include "List.h"
#include "Queue.h"
#include "Stack.h"

int main()
{
	List<int> a;
	Queue<int> h;
	Stack<int> s;
	int b, c, d;

	std::cin >> b >> c >> d;
	a.pushBack(b);
	a.pushBack(c);
	a.pushFront(d);

	a.print();
	system("PAUSE");

	std::cout << a.size();
	system("PAUSE");

	a.delFront();
	a.print();
	system("PAUSE");

	a.delFront();
	a.print();
	system("PAUSE");

	std::cout << "\n\nQueue:\n";
	h.enqueue(d);
	h.enqueue(b);
	h.enqueue(c);

	h.print();
	system("PAUSE");

	std::cout << h.size();
	system("PAUSE");

	std::cout << h.dequeue();
	system("PAUSE");
	h.print();
	system("PAUSE");

	std::cout << h.front();
	system("PAUSE");

	if (h.isEmpty())
		std::cout << "Jest puste\n";
	else
		std::cout << "Nie jest puste\n";
	system("PAUSE");

	h.remove();
	if (h.isEmpty())
		std::cout << "Jest puste\n";
	else
		std::cout << "Nie jest puste\n";
	system("PAUSE");

	std::cout << h.dequeue();
	system("PAUSE");

	std::cout << "\n\nStack:\n";
	s.push(c);
	s.push(b);
	s.push(d);

	s.print();
	system("PAUSE");

	s.pop();
	s.print();
	system("PAUSE");

	s.push(d);
	s.print();
	system("PAUSE");

	s.pop();
	s.print();
	system("PAUSE");

	s.remove();
	s.print();
	system("PAUSE");

	s.pop();
	s.print();
	system("PAUSE");
}