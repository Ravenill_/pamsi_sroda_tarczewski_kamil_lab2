#pragma once

template <typename Type>
class mNode
{
private:
	Type data;
	mNode<Type> *next;
	mNode<Type> *prev;


public:
	mNode<Type>* getNext() { return next; }
	void setNext(mNode<Type> *a) { next = a; }
	mNode<Type>* getPrev() { return prev; }
	void setPrev(mNode<Type> *a) { prev = a; }
	Type getData() { return data; }
	void setData(Type a) { data = a; }
};