#pragma once
#include "List.h"

template <typename Type>
class Stack : public List<Type>
{
private:
	Node<Type> *first;

	//przeciążenie, by wyłączyć
	void pushBack(Type a) {}

public:
	Stack();
	~Stack();

	void push(Type a);
	Type pop();
	Type top();
};

#include "Stack.tpp"
