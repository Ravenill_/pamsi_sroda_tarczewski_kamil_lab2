#pragma once
#include "List.h"

template <typename Type>
class sList : public List<Type>
{
private:
	Node<Type> *first;
	Node<Type> *last;

public:
	void usunDup();
};

#include "sList.tpp"