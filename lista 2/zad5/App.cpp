#include "Deque.h"
#include "App.h"
#include <iostream>
#include <cstdlib>

App::App()
{

}

App::~App()
{

}

void App::run()
{
	std::cout << "Podaj wyraz: \n";
	std::cin >> word;
	if (!std::cin.good())//g�upkoodporno��
		error();
	else
	{
		system("CLS");
		std::cout << "Wyraz: " << word << ((jestPal(word.c_str()) == true) ? " jest palindromem\n" : " NIE jest palindromem\n");
		system("PAUSE");
	}
}

bool App::jestPal(const char *charword)
{
	Deque<char> pal;
	//za�aduj tekst do tablicy charowej - deque
	for (size_t i = 0; i < strlen(charword); i++)
		pal.wstawOstatni(charword[i]);
	
	while (!pal.isEmpty())
	{
		if (pal.rozmiar() <= 1)//Koniec ci�gu znak�w
			return true;
		if (pal.usunPierwszy() != pal.usunOstatni())//Przyr�wnanie
			return false;
	}
	return true;
}

void error()
{
	system("CLS");
	std::cerr << "Error! Wpisano zla opcje/wartosc\n";
	std::cin.clear();
	std::cin.sync();
	std::cin.ignore(1000, '\n');
	system("PAUSE");
}