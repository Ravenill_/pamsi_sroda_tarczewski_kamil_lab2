#pragma once

template <typename Type>
struct uList
{
	uList<Type> *next;
	Type data;
};

template <typename Type>
class List
{
private:
	uList<Type> *first;//wska�nik na pierwszy element

public:
	List();
	~List();

	int size();
	void print();
	void pushFront(Type a);
	void pushBack(Type a);
	void delFront();
	void delBack();
	void remove();
	void usunDup();
};

#include "List.tpp"