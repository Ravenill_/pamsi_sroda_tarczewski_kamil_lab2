#include "Displayer.h"
#include <cstdlib>
#include <iostream>

using namespace std;

void getMenu()
{
	system("CLS");
	cout << "--MENU GLOWNE--\n";
	cout << "[1] LISTA\n";
	cout << "[2] KOLEJKA\n";
	cout << "[0] UCIECZKA Z PROGRAMU\n";
}

void getMenu1()
{
	system("CLS");
	cout << "--MENU LISTY--\n";
	cout << "[1] Dodaj element na poczatku\n";
	cout << "[2] Dodaj element na koncu\n";
	cout << "[3] Usun element z poczatku\n";
	cout << "[4] Usun cala zawartosc listy\n";
	cout << "[5] Wyswietl zawartosc listy\n";
	cout << "[0] Powrot\n";
}

void getMenu2()
{
	system("CLS");
	cout << "--MENU KOLEJKI--\n";
	cout << "[1] Dodaj element\n";
	cout << "[2] Usun i wyswietl element z poczatku\n";
	cout << "[3] Wyswietl pierwszy element\n";
	cout << "[4] Wyswietl zawartosc kolejki\n";
	cout << "[5] Usun cala zawartosc kolejki\n";
	cout << "[0] Powrot\n";
}
