#pragma once

template <typename Type>
class Node
{
private:
	Type data;
	Node<Type> *next;

public:
	Node<Type>* getNext() { return next; }
	void setNext(Node<Type> *a) { next = a; }
	Type getData() { return data; }
	void setData(Type a) { data = a; }
};