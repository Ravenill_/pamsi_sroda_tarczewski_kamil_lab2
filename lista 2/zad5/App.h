#pragma once
#include <string>

class App
{
private:
	std::string word;

public:
	App();
	~App();

	bool jestPal(const char*);
	void run();
};

void error();