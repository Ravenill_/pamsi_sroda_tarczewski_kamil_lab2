#include <iostream>
#include <string>

template<typename Type>
List<Type>::List()
{
	first = NULL;//zainicjowanie pierwszego wska�nika
	last = NULL;
}

template<typename Type>
List<Type>::~List()
{
	while (first != NULL)//usuwj front, p�ki nie dojdzie do ko�ca
		delFront();
}

template<typename Type>
int List<Type>::size()
{
	int size = 0;

	//licz, p�ki nie przekieruje Ci� na NULLA z ostatniego elementu
	for (Node<Type> *temp = first; temp != NULL; temp = (*temp).getNext())
		size += 1;

	return size;
}

template<typename Type>
void List<Type>::print()
{
	//wy�wietlaj, p�ki nie przekieruje Ci� na NULLA z ostatniego elementu
	for (Node<Type> *temp = first; temp != NULL; temp = (*temp).getNext())
		std::cout << (*temp).getData() << "\n";
}

template<typename Type>
void List<Type>::pushFront(Type a)
{
	//nowy element
	Node<Type> *newElem;
	newElem = new Node<Type>;

	//wrzu� na prz�d
	(*newElem).setNext(first);
	(*newElem).setData(a);

	if (first == NULL)
		last = newElem;

	first = newElem;

}

template<typename Type>
void List<Type>::pushBack(Type a)
{
	//deklaracja i inicjalizacja
	Node<Type> *newElem, *temp;

	temp = first;
	newElem = new Node<Type>;
	(*newElem).setNext(NULL);
	(*newElem).setData(a);

	if (temp == NULL)
		first = newElem;
	else
	{
		temp = last;
		(*temp).setNext(newElem);
	}

	last = newElem;
}

template<typename Type>
void List<Type>::delFront()
{
	try
	{
		Node<Type> *temp = first;

		if (temp != NULL)//usu�, je�li jest co� na li�cie
		{
			first = (*temp).getNext();
			delete temp;
		}
		else
		{
			std::string w = "EmptyListException";
			throw w;
		}
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return;
	}
	
}

template<typename Type>
void List<Type>::remove()
{
	while (first)//jak przy dekonstruktorze
		delFront();
}

template<typename Type>
bool List<Type>::isEmpty()
{
	if (first == NULL)
		return true;
	else
		return false;
}