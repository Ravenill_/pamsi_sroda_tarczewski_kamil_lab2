#include "App.h"
#include <iostream>
#include <cstdlib>
#include "sList.h"

sList<std::string> palList; //deklaracja globalna, wed�ug �yczenia

App::App()
{

}

App::~App()
{

}

//Main App
void App::run()
{
	std::cout << "Podaj wyraz: \n";
	std::cin >> word;
	if (!std::cin.good())//zabezpieczenie
		error();
	else
	{
		perm(static_cast<int>(word.length()));//generuj permutacje palindromowe
		palList.usunDup();//usu� duplikaty

		system("CLS");
		std::cout << "Palindromy wyrazu: " << word << ", jakie powstaly na skutek permutacji:\n";
		palList.print();
		system("PAUSE");
	}
}

void App::perm(int size)
{
	if (size == 0)
	{
		if (jestPal(word))//sprawd�, czy palindorm
			palList.pushFront(word);//pushBack jest wolniejsze
	}
	else
	{
		for (int i = 0; i < size; i++)
		{
			std::swap(word[i], word[size-1]); //zmiana 
			perm(size - 1); //je�li dosz�o do ko�ca - zapisz, je�li nie zmieniaj dalej
			std::swap(word[i], word[size-1]); //powr�t do domy�lnej konfigruacji
		}
	}

}

bool App::jestPal(std::string testStr)
{
	if (testStr.length() <= 1)//Koniec ci�gu znak�w
		return true;//jest Pal
	if (testStr[0] == testStr[testStr.length() - 1])//Przyr�wnanie
		return jestPal(testStr.substr(1, testStr.length() - 2));
	else
		return false;
}

//Plujka
void error()
{
	system("CLS");
	std::cerr << "Error! Wpisano zla opcje/wartosc\n";
	std::cin.clear();
	std::cin.sync();
	std::cin.ignore(1000, '\n');
	system("PAUSE");
}