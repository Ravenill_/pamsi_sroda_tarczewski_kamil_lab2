#pragma once
#include "mNode.h"

template <typename Type>
class Deque
{
private:
	mNode<Type> *first;//wska�nik na pierwszy element
	mNode<Type> *last;//wska�nik na ostatni element

public:
	Deque();
	~Deque();

	int rozmiar();
	bool isEmpty();
	Type pierwszy();
	Type ostatni();
	void wstawPierwszy(Type obj);
	Type usunPierwszy();
	void wstawOstatni(Type obj);
	Type usunOstatni();
};

#include "Deque.tpp"
