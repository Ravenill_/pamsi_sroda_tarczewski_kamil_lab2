#include <iostream>

template<typename Type>
Queue<Type>::Queue()
{
	first = NULL;
	last = NULL;
}

template<typename Type>
Queue<Type>::~Queue()
{
	while (first)
		dequeue();
}

template<typename Type>
void Queue<Type>::enqueue(Type a)
{
	uQueue<Type> *newElem, *temp;

	temp = first;
	newElem = new uQueue<Type>;
	(*newElem).next = NULL;
	(*newElem).data = a;

	if (temp == NULL)
		first = newElem;
	else
	{
		temp = last;
		(*temp).next = newElem;
	}

	last = newElem;
}

template<typename Type>
Type Queue<Type>::dequeue()
{
	uQueue<Type> *temp = first;
	Type result;

	if (temp)
	{
		result = (*temp).data;
		first = (*temp).next;
		delete temp;
	}
	else
	{
		std::cerr << "EmptyQueueException\n";
		return 0;
	}

	return result;
}

template<typename Type>
Type Queue<Type>::front()
{
	uQueue<Type> *temp = first;
	Type result;

	if (temp)
		result = (*temp).data;
	else
	{
		std::cerr << "EmptyQueueException\n";
		return 0;
	}

	return result;
}

template<typename Type>
int Queue<Type>::size()
{
	uQueue<Type> *temp = first;
	int size = 0;

	while (temp)
	{
		size += 1;
		temp = (*temp).next;
	}


	return size;
}

template<typename Type>
bool Queue<Type>::isEmpty()
{
	if (first == NULL)
		return true;
	else
		return false;
}

template<typename Type>
void Queue<Type>::print()
{
	uQueue<Type> *temp = first;
	
	while (temp)
	{
		std::cout << (*temp).data << "\n";
		temp = (*temp).next;
	}
}

template<typename Type>
void Queue<Type>::remove()
{
	while (first)
		dequeue();
}