#pragma once
#include <iostream>

template <typename Type>
Deque<Type>::Deque()
{
	first = NULL;
	last = NULL;
}

template <typename Type>
Deque<Type>::~Deque()
{

}

template <typename Type>
int Deque<Type>::rozmiar()
{
	int size = 0;

	//licz, p�ki nie przekieruje Ci� na NULLA z ostatniego elementu
	for (mNode<Type> *temp = first; temp != NULL; temp = (*temp).getNext())
		size += 1;

	return size;
}

template <typename Type>
bool Deque<Type>::isEmpty()
{
	if (first == NULL)
		return true;
	else
		return false;
}

template <typename Type>
Type Deque<Type>::pierwszy()
{
	try
	{
		mNode<Type> *temp = first;
		Type result;

		if (temp != NULL)
			result = (*temp).getData();
		else
		{
			std::string w = "EmptyDequeException";
			throw w;
		}
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template <typename Type>
Type Deque<Type>::ostatni()
{
	try
	{
		mNode<Type> *temp = last;
		Type result;

		if (temp != NULL)
			result = (*temp).getData();
		else
		{
			std::string w = "EmptyDequeException";
			throw w;
		}
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template <typename Type>
void Deque<Type>::wstawPierwszy(Type obj)
{
	//nowy element
	mNode<Type> *newElem;
	newElem = new mNode<Type>;

	//wrzu� na prz�d
	(*newElem).setNext(first);
	(*newElem).setPrev(NULL);
	(*newElem).setData(obj);

	if (first == NULL)
		last = newElem;
	else
		(*(*newElem).getNext()).setPrev(newElem);

	first = newElem;
}

template <typename Type>
Type Deque<Type>::usunPierwszy()
{
	try
	{
		mNode<Type> *temp = first;
		Type result;

		if (temp != NULL)//usu�, je�li jest co� na li�cie
		{
			result = (*first).getData();
			first = (*temp).getNext();
			if (first != NULL)
				(*first).setPrev(NULL);
			else
				last = NULL;
			delete temp;
		}
		else
		{
			std::string w = "EmptyDequeException";
			throw w;
		}
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template <typename Type>
void Deque<Type>::wstawOstatni(Type obj)
{
	//deklaracja i inicjalizacja
	mNode<Type> *newElem, *temp;

	temp = first;
	newElem = new mNode<Type>;
	(*newElem).setNext(NULL);
	(*newElem).setPrev(NULL);
	(*newElem).setData(obj);

	if (temp == NULL)
		first = newElem;
	else
	{
		temp = last;
		(*temp).setNext(newElem);
		(*newElem).setPrev(temp);
	}

	last = newElem;
}

template <typename Type>
Type Deque<Type>::usunOstatni()
{
	try
	{
		mNode<Type>* temp = last;
		Type result;

		if (temp != NULL)//usu�, je�li jest co� na li�cie
		{
			result = (*last).getData();
			last = (*temp).getPrev();
			if (last != NULL)
				(*last).setNext(NULL);
			else
				first = NULL;
			delete temp;
		}
		else
		{
			std::string w = "EmptyDequeException";
			throw w;
		}
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}