#pragma once
#include "ListCore.h"
#include "QueueCore.h"

enum Stat { MENU, MENU1, MENU2, END, LIST, QUEUE };

class App
{
private:
	int option, arg;
	Stat state;
	QueueCore queue; //opcje z tab dwuwymiarową CORE1
	ListCore list; //opcje z tab jednowymiarową CORE2


public:
	App();
	~App();

	void run(); //uruchomienie programu
	void menu(); //główne
	void menu1(); //jednowymiarowa
	void menu2(); //dwuwymiarowa

	void run_list(); //Lista
	void run_queue(); //Kolejka
};

void error();