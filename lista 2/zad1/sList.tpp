#pragma once
#include <iostream>

//funkcja usuwaj�ca duplikat
template<typename Type>
void sList<Type>::usunDup()
{
	//bierz elementy z g�ry
	first = List<Type>::getFirst();
	last = List<Type>::getLast();
	
	Node<Type> *temp, *tempElem;
	temp = first;

	for (Node<Type> *loop = first; loop != NULL; loop = (*loop).getNext()) //loop... Og�lna p�tla (wolniejsza)
	{
		temp = loop; //reset p�tli

		while ((*temp).getNext() != NULL)//i temp (skocz�ca ca�y czas w prz�d i na resecie wracaj�ca do loopa)
		{
			if ((*loop).getData() == (*(*temp).getNext()).getData())//przyr�wnaj loop i temp - element z przodu do nast�pnych po nim
			{
				//usuni�cie powt�rzenia
				tempElem = (*temp).getNext();
				(*temp).setNext((*tempElem).getNext());
				delete tempElem;
			}
			else //je�li nie ma powt�rzenia wrzu� nast�pny element
				temp = (*temp).getNext();
		}
	}
}