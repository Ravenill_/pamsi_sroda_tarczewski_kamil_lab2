#include <iostream>

template<typename Type>
List<Type>::List()
{
	first = NULL;//zainicjowanie pierwszego wska�nika
}

template<typename Type>
List<Type>::~List()
{
	while (first != NULL)//usuwj front, p�ki nie dojdzie do ko�ca
		delFront();
}

template<typename Type>
int List<Type>::size()
{
	int size = 0;

	//licz, p�ki nie przekieruje Ci� na NULLA z ostatniego elementu
	for (uList<Type> *temp = first; temp != NULL; temp = (*temp).next)
		size += 1;

	return size;
}

template<typename Type>
void List<Type>::print()
{
	//wy�wietlaj, p�ki nie przekieruje Ci� na NULLA z ostatniego elementu
	for (uList<Type> *temp = first; temp != NULL; temp = (*temp).next)
		std::cout << (*temp).data << "\n";
}

template<typename Type>
void List<Type>::pushFront(Type a)
{
	//nowy element
	uList<Type> *newElem;
	newElem = new uList<Type>;

	//wrzu� na prz�d
	(*newElem).next = first;
	(*newElem).data = a;
	first = newElem;
}

template<typename Type>
void List<Type>::pushBack(Type a)
{
	//deklaracja i inicjalizacja
	uList<Type> *temp, *newElem;

	newElem = new uList<Type>;
	(*newElem).next = NULL;
	(*newElem).data = a;

	temp = first;
	if (temp != NULL)
	{
		while ((*temp).next != NULL)//przejd� na koniec i...
			temp = (*temp).next;
		(*temp).next = newElem;//przypisz nowo powsta�y element
	}
	else
		first = newElem;//je�li nie ma nic na li�cie
}

template<typename Type>
void List<Type>::delFront()
{
	uList<Type> *temp = first;

	if (temp != NULL)//usu�, je�li jest co� na li�cie
	{
		first = (*temp).next;
		delete temp;
	}
}

template<typename Type>
void List<Type>::delBack()
{
	uList<Type> *temp = first;

	if (temp != NULL)//je�li jest co� na li�cie...
	{
		if ((*temp).next != NULL)//...i je�li jest co� dalej...
		{
			while ((*(*temp).next).next != NULL)//...kr�� tak daleko, by doj�� do drugiego od ko�ca
				temp = (*temp).next; 

			delete (*temp).next; //wska�nik na ostatni element
			(*temp).next = NULL;
		}
		else //je�li jest tylko jeden element
		{
			delete temp;
			first = NULL;
		}
	}
}

template<typename Type>
void List<Type>::remove()
{
	while (first)//jak przy dekonstruktorze
		delFront();
}

template<typename Type>
void List<Type>::usunDup()
{
	uList<Type> *temp, *tempElem;
	temp = first;

	for (uList<Type> *loop = first; loop != NULL; loop = (*loop).next) //przyr�wnuj loop...
	{
		temp = loop; //reset p�tli

		while ((*temp).next != NULL)//...do temp
		{
			if ((*loop).data == (*(*temp).next).data)
			{
				//usuni�cie powt�rzenia
				tempElem = (*temp).next;
				(*temp).next = (*tempElem).next;
				delete tempElem;
			}
			else //je�li nie ma powt�rzenia wrzu� nast�pny element
				temp = (*temp).next;
		}
	}
}