#include "ListCore.h"
#include <iostream>

ListCore::ListCore()
{

}

ListCore::~ListCore()
{

}

void ListCore::run(int opt, int arg)
{
	switch (opt)
	{
	case 1: //PushFront
		a.pushFront(arg);
		break;

	case 2: //PushBack
		a.pushBack(arg);
		break;

	case 3: //DelFront
		a.delFront();
		break;

	case 4: //Remove
		a.remove();
		break;

	case 5: //Print
		a.print();
		system("PAUSE");
		break;
	}
}