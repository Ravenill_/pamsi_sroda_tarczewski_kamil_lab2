#pragma once
#include <iostream>

template<typename Type>
Stack<Type>::Stack()
{
	first = NULL;
}

template<typename Type>
Stack<Type>::~Stack()
{
	while (first)
		pop();
}

template<typename Type>
void Stack<Type>::push(Type a)
{
	pushFront(a);
	first = List<Type>::getFirst();
}

template<typename Type>
Type Stack<Type>::pop()
{
	try
	{
		Node<Type> *temp = first;
		Type result;

		if (temp != NULL)
		{
			result = (*temp).getData();
			delFront();
			first = List<Type>::getFirst();
		}
		else
		{
			std::string w = "EmptyQueueException";
			throw w;
		}
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template<typename Type>
Type Stack<Type>::top()
{
	try
	{
		Node<Type> *temp = first;
		Type result;

		if (temp != NULL)
			result = (*temp).getData();
		else
		{
			std::string w = "EmptyQueueException";
			throw w;
		}
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}