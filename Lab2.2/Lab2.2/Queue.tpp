#include <iostream>

template<typename Type>
Queue<Type>::Queue()
{
	first = NULL;
	last = NULL;
}

template<typename Type>
Queue<Type>::~Queue()
{
	while (first != NULL)//je�li co� jest, to si� kryn�
		dequeue();
}

template<typename Type>
void Queue<Type>::enqueue(Type a)
{
	//funckja z listy
	pushBack(a);
	//przypisz do wska�nik�w adresy z listy
	first = List<Type>::getFirst();
	last = List<Type>::getLast();
}

template<typename Type>
Type Queue<Type>::dequeue()
{
	try
	{
		Node<Type> *temp = first;
		Type result;

		if (temp != NULL)//je�li niepusta
		{
			result = (*temp).getData();//we� dane
			delFront();//usu� prz�d
			//przypisz wska�niki
			first = List<Type>::getFirst();
			last = List<Type>::getLast();
		}
		else//plujka
		{
			std::string w = "EmptyQueueException";
			throw w;
		}
		return result;
	}
	catch(std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template<typename Type>
Type Queue<Type>::front()
{
	try
	{
		Node<Type> *temp = first;
		Type result;

		if (temp != NULL)//je�li niepusta
			result = (*temp).getData();//wyrzu� data
		else//albo pluj b��dami
		{
			std::string w = "EmptyQueueException";
			throw w;
		}
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template<typename Type>
void Queue<Type>::remove()
{
	List<Type>::remove();//we� metod� z klasy wy�ej

	//przypisz wska�niki
	first = List<Type>::getFirst();
	last = List<Type>::getLast();
}
