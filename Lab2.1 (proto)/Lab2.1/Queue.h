#pragma once
#include "List.h"

template <typename Type>
class Queue : public List<Type>
{
private:
	Node<Type> *first;
	Node<Type> *last;

	//przeciążenie, by wyłączyć
	void pushFront(Type a) {}

public:
	Queue();
	~Queue();

	void enqueue(Type a);
	Type dequeue();
	Type front();
	void remove();
};

#include "Queue.tpp"