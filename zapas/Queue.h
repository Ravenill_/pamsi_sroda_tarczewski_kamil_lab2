#pragma once

template<typename Type>
struct uQueue
{
	uQueue<Type> *next;
	Type data;
};

template <typename Type>
class Queue
{
private:
	uQueue<Type> *first;
	uQueue<Type> *last;

public:
	Queue();
	~Queue();

	void enqueue(Type a);
	Type dequeue();
	Type front();
	int size();
	bool isEmpty();
	void print();
	void remove();
};

#include "Queue.tpp"