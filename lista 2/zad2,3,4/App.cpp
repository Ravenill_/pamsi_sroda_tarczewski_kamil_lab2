#include "App.h"
#include "Displayer.h"
#include <iostream>
#include <cstdlib>

App::App()
{
	state = MENU; //Ustawienie zmiennej informuj�cej gdzie si� znajdujemy na MENU
	option = 0;
}

App::~App()
{

}

//G��wny program
void App::run()
{
	//obs�uga programu
	while (state != END)
	{
		switch (state)
		{
		case Stat::MENU://Menu g��wne
			menu();
			break;

		case Stat::MENU1: //Menu listy
			menu1();
			break;

		case Stat::MENU2: //Menu kolejki
			menu2();
			break;

		case Stat::LIST: //Kolejka
			run_list();
			break;

		case Stat::QUEUE: //Lista
			run_queue();
			break;

		default:

			break;
		}
	}
}

//Main Menu
void App::menu()
{
	int opt;

	while (state == MENU)
	{
		//wy�wietlenie menu
		getMenu();

		//odczytanie opcji, g�upkoodporno��
		std::cin >> opt;
		if (!std::cin.good())
			error();
		else
		{
			switch (opt)
			{
			case 1://lista
				state = MENU1;
				break;

			case 2://kolejka
				state = MENU2;
				break;

			case 0://wyj�cie
				state = END;
				break;

			default:
				system("CLS");
				std::cout << "Error! Wpisano zla opcje\n";
				system("PAUSE");
				break;
			}//G��wna obs�uga menu
		}//Else
	}//While
}

//Menu Listy
void App::menu1()
{
	int opt;//opcja, co robi�
	int opt2 = 0;//argument dodawany do tablicy

	while (state == MENU1)
	{
		//wy�wietlenie menu
		getMenu1();

		//odczytanie opcji, g�upkoodporno��
		std::cin >> opt;
		if (!std::cin.good())
			error();
		else
		{
			switch (opt)
			{
			case 1:
				std::cout << "\nPodaj element, by wprowadzic go do listy:\n";
				std::cin >> opt2;
				if (std::cin.good())
				{
					state = LIST;
					option = opt;
					arg = opt2;
				}
				else
					error();
				break;

			case 2:
				std::cout << "\nPodaj element, by wprowadzic go do listy:\n";
				std::cin >> opt2;
				if (std::cin.good())
				{
					state = LIST;
					option = opt;
					arg = opt2;
				}
				else
					error();
				break;

			case 3:
				state = LIST;
				option = opt;
				break;

			case 4:
				state = LIST;
				option = opt;
				break;

			case 5:
				state = LIST;
				option = opt;
				break;

			case 6:
				state = LIST;
				option = opt;
				break;

			case 0:
				state = MENU;
				break;

			default:
				system("CLS");
				std::cout << "Error! Wpisano zla opcje\n";
				system("PAUSE");
				break;
			}//G��wna obs�uga menu
		}//Else
	}//While
}

//Menu Kolejki
void App::menu2()
{
	int opt;//opcja co robi�
	int opt2 = 0;//argument dodaj�cy co� do kolejki

	while (state == MENU2)
	{
		//wy�wietlenie menu
		getMenu2();

		//odczytanie opcji, g�upkoodporno��
		std::cin >> opt;
		if (!std::cin.good())
			error();
		else
		{
			switch (opt)
			{
			case 1:
				std::cout << "\nPodaj element, by wprowadzic go do listy:\n";
				std::cin >> opt2;
				if (std::cin.good())
				{
					state = QUEUE;
					option = opt;
					arg = opt2;
				}
				else
					error();
				break;

			case 2:
				state = QUEUE;
				option = opt;
				break;

			case 3:
				state = QUEUE;
				option = opt;
				break;

			case 4:
				state = QUEUE;
				option = opt;
				break;

			case 5:
				state = QUEUE;
				option = opt;
				break;

			case 0:
				state = MENU;
				break;

			default:
				system("CLS");
				std::cout << "Error! Wpisano zla opcje\n";
				system("PAUSE");
				break;
			}//G��wna obs�uga menu
		}//Else
	}//While
}

//Modu� z List�
void App::run_list()
{
	system("CLS");

	list.run(option, arg);

	//wyzerowanie i powr�t do menu
	option = 0;
	arg = 0;
	state = MENU1;
}

//Modu� z Kolejk�
void App::run_queue()
{
	system("CLS");

	queue.run(option, arg);

	//wyzerowanie i powr�t do menu
	option = 0;
	arg = 0;
	state = MENU2;
}

//Plujka z b��dami
void error()
{
	system("CLS");
	std::cerr << "Error! Wpisano zla opcje/wartosc\n";
	std::cin.clear();
	std::cin.sync();
	std::cin.ignore(1000, '\n');
	system("PAUSE");
}